export const taskData = [
  { id: "1", detail: "Do Home Work" },
  { id: "2", detail: "Feed The Cat" },
  { id: "3", detail: "Feed The Dog" },
  { id: "4", detail: "Feed The Fish" },
  { id: "5", detail: "Feed The Lion" },
  { id: "6", detail: "Feed The Tiger" },
];
