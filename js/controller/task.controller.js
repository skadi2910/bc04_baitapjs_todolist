export const getTaskDetail = () => {
  let _id = Math.floor(Math.random() * 100 + 10);
  const detail = document.getElementById("newTask").value;
  return { detail: detail, id: `${_id}` };
};

export const renderTaskList = (data) => {
  let contentHtml = "";
  data.forEach((item) => {
    let contentLi = `<li>
    <p>${item.detail}</p>
    <div>
      <i class="fa fa-trash" id="deleteTask" onClick="deteleItem(${item.id})"></i>
      <i class="fa fa-check" id="completeTask"  onClick="completeTask(${item.id})"></i>
    </div>
  </li>`;
    contentHtml += contentLi;
  });
  document.getElementById("todo").innerHTML = contentHtml;
};

export const renderCompletedTask = (data) => {
  let contentHtml = "";
  data.forEach((item) => {
    let contentLi = `<li>
    <p>${item.detail}</p>
    <div>
      <i class="fa fa-trash" id="deleteTask" onClick="deteleCompletedItem(${item.id})"></i>
      <i class="fa fa-check" id="completedTask"></i>
    </div>
  </li>`;
    contentHtml += contentLi;
  });
  document.getElementById("completed").innerHTML = contentHtml;
};
