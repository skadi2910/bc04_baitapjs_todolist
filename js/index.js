// import { toDoList } from "./toDoList.js";
import {
  getTaskDetail,
  renderTaskList,
  renderCompletedTask,
} from "./controller/task.controller.js";

const BASE_URL = "https://62f24e04b1098f15081009f0.mockapi.io",
  TODO_LOCALSTORAGE = "TODO_LIST",
  COMPLETED_LOCALSTORAGE = "COMPLETED_LIST";

const getTaskList = async () => {
  try {
    const response = await axios.get(`${BASE_URL}/tasks`);
    // renderTaskList(response.data);
    return response.data;
  } catch (error) {
    console.log("error: ", error);
  }
};
let toDoList = await getTaskList(),
  completedList = [];
const setLocalStorageToDoList = (data) => {
  const todoListJSON = JSON.stringify(data);
  localStorage.setItem(TODO_LOCALSTORAGE, todoListJSON);
};
const setLocalStoragecompletedList = (data) => {
  const completedListJSON = JSON.stringify(data);
  localStorage.setItem(COMPLETED_LOCALSTORAGE, completedListJSON);
};
const getLocalStorage = () => {
  const todoListJSON = localStorage.getItem(TODO_LOCALSTORAGE);
  const compeletedListJSON = localStorage.getItem(COMPLETED_LOCALSTORAGE);
  if (todoListJSON == null || Object.keys(todoListJSON).length <= 2) {
    setLocalStorageToDoList(toDoList);
    renderTaskList(toDoList);
  } else {
    toDoList = JSON.parse(todoListJSON);
    renderTaskList(toDoList);
  }
  if (compeletedListJSON != null) {
    completedList = JSON.parse(compeletedListJSON);
    renderCompletedTask(completedList);
  }
};
const addItem = () => {
  let task = getTaskDetail();
  if (task.detail != "") {
    toDoList.push(task);
    setLocalStorageToDoList(toDoList);
    renderTaskList(toDoList);
  }
};
const deteleItem = (id) => {
  let index = toDoList.findIndex((item) => {
    console.log("item.id: ", item.id);
    return item.id.toString().toLowerCase() === id.toString().toLowerCase();
  });
  if (index != -1) {
    toDoList.splice(index, 1);
    setLocalStorageToDoList(toDoList);
    renderTaskList(toDoList);
  }
};
const deteleCompletedItem = (id) => {
  let index = completedList.findIndex((item) => {
    return item.id * 1 === id;
  });
  completedList.splice(index, 1);
  setLocalStoragecompletedList(completedList);
  renderCompletedTask(completedList);
};
const completeTask = (id) => {
  let index = toDoList.findIndex((item) => {
    return item.id * 1 === id;
  });
  completedList.push({
    detail: toDoList[index].detail,
    id: completedList.length + 1,
  });
  toDoList.splice(index, 1);
  setLocalStorageToDoList(toDoList);
  console.log("toDoList: ", toDoList);
  setLocalStoragecompletedList(completedList);
  renderTaskList(toDoList);
  renderCompletedTask(completedList);
};
const sortAscending = (data) => {
  data.sort((a, b) =>
    a.detail.toLowerCase().localeCompare(b.detail.toLowerCase())
  );
  renderTaskList(data);
};
const sortDescending = (data) => {
  data.sort((a, b) =>
    b.detail.toLowerCase().localeCompare(a.detail.toLowerCase())
  );
  renderTaskList(data);
};
function initialize() {
  getLocalStorage();

  window.deteleItem = deteleItem;
  window.completeTask = completeTask;
  window.deteleCompletedItem = deteleCompletedItem;

  document.getElementById("addItem").addEventListener("click", addItem);
  document.getElementById("two").addEventListener("click", function () {
    sortAscending(toDoList);
  });
  document.getElementById("three").addEventListener("click", function () {
    sortDescending(toDoList);
  });
}

initialize();
